apt-get update
apt-get install -y build-essential \
  libreadline-dev \
  zlib1g-dev \
  flex \
  bison \
  libxml2-dev \
  libxslt-dev \
  libssl-dev \
  libxml2-utils \
  xsltproc \
  dpkg-dev \
  curl \
  git \
  debhelper \
  dh-exec \
  docbook-xml \
  docbook-xsl \
  gettext \
  krb5-multidev \
  libedit-dev \
  libipc-run-perl \
  libldap2-dev \
  libpam0g-dev \
  libperl-dev \
  libselinux1-dev \
  libsystemd-dev \
  pkg-config \
  python3-dev \
  systemtap-sdt-dev \
  tcl-dev \
  uuid-dev \
  clang-6.0 \
  llvm-6.0 \
  pkg-create-dbgsym

PG_VER=$1
PG_TAG=$2
curl -O https://ftp.postgresql.org/pub/source/v"$PG_VER"/postgresql-"$PG_VER".tar.gz
tar -zxvf postgresql-"$PG_VER".tar.gz

git clone -b debian/"$PG_TAG" https://salsa.debian.org/postgresql/postgresql.git
cp -rf postgresql/debian postgresql-"$PG_VER"

cd postgresql-"$PG_VER"
echo 9 > debian/compat
patch -p1 < ../patches/debian.patch

echo '---------- debian/control'
cat debian/control
echo '---------- debian/control.rej'
cat debian/control.rej
echo '---------- debian/rules'
cat debian/rules
echo '---------- debian/rules.rej'
cat debian/rules.rej


DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -rfakeroot -b -uc -us
