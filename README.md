# Build postgres-12.9-1 for Ubuntu 16

https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/14736

Based on @stanhu's https://gitlab.com/gitlab-com/gl-infra/postgresql-patches

## Changes from ``postgresql-patches``

* Removed subtransaction patches
* Tracked down the [appropriate tag ](https://salsa.debian.org/postgresql/postgresql/-/tags/debian%2F12.9-1)
* Backported `-pie` flags to compiler and linker for successful build

## Instructions

* Copy the [postgres-patches](https://gitlab.com/gitlab-com/gl-infra/postgresql-patches) or this project into its own project (stay consistent with naming: `postgresql-version` in the [`database/postgresql`](https://gitlab.com/gitlab-com/gl-infra/database/postgresql/) hierarchy
* Edit the `build.sh` script to:
  * update the Postgres version (`PGVER`)
  * find the appropriate tag to clone from https://salsa.debian.org/postgresql/postgresql.git
* Update the patch file to produce the usable `debian/control` and `debian/rules` files; check out pipeline output for errors. In particular, watch out for patch application failures and compilation errors
* Let the pipelines do the rest
